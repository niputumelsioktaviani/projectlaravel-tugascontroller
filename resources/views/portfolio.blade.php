@extends('layout')

@section('judul')
    Portfolio
@endsection

@section('konten')
    <!--Gallery-->
    <section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
        <h1>MY <span>PORTFOLIO</span></h1>
        <span class="title-bg">GALLERY</span>
    </section>
    <section id="gallery" class="Gallery">
        <div class="container-gallery">

            <!--baris 1-->
            <div class=" row service-box">
                <div class="col-md-3 mb-4">
                    <div class="single-service">
                        <img src="{{ asset('style/img/gal1.jpg') }}" alt="Card Image cap">
                        <div class="service-desc">
                            <h3>LPJ BEM 2021</h3>
                            <hr>
                            <p>Melsi Oktaviani sedang melaksanakan kegiatan LPJ BEM 2021.</p>
                        </div>
                    </div>
                </div>
                <!--baris 2-->
                <div class="col-md-3">
                    <div class="single-service">
                        <div>
                            <img src="{{ asset('style/img/gal2.jpg') }}" alt="Card Image cap">
                            <div class="service-desc">
                                <h3>Camping</h3>
                                <hr>
                                <p> Gambar ini Melsi sedang camping di Pinggan.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!--baris 3-->
                <div class="col-md-3">
                    <div class="single-service">
                        <img src="{{ asset('style/img/gal3.jpg') }}" alt="Card Image cap">
                        <div class="service-desc">
                            <h3>PAT 2020</h3>
                            <hr>
                            <p> Gambar ini Melsi sedang mengikuti PAT 2020 di kampus.</p>
                        </div>
                    </div>
                </div>
                <!--baris 4-->
                <div class="col-md-3">
                    <div class="single-service">
                        <img src="{{ asset('style/img/gal4.jpg') }}" alt="Card Image cap">
                        <div class="service-desc">
                            <h3>Kunjungan ke FBS</h3>
                            <hr>
                            <p>Gambar ini Melsi sedang kunjungan pameran di FBS.</p>
                        </div>
                    </div>
                </div>
                <!--baris 5-->
                <div class="col-md-3">
                    <div class="single-service">
                        <img src="{{ asset('style/img/gal5.jpg') }}" alt="Card Image cap">
                        <div class="service-desc">
                            <h3>Holiday</h3>
                            <hr>
                            <p>Gambar ini Melsi sedang holiday bersama teman-temannya ke krisna pada tahun 2020.</p>
                        </div>
                    </div>
                </div>
                <!--baris 6-->
                <div class="col-md-3">
                    <div class="single-service">
                        <img src="{{ asset('style/img/gal6.jpg') }}" alt="Card Image cap">
                        <div class="service-desc">
                            <h3>Penelitian</h3>
                            <hr>
                            <p>Gambar ini Melsi sedang melaksanakan penelitian untuk tugas kelompok THK.</p>
                        </div>
                    </div>
                </div>
                <!--baris 7-->
                <div class="col-md-3">
                    <div class="single-service">
                        <img src="{{ asset('style/img/gal7.jpg') }}" alt="Card Image cap">
                        <div class="service-desc">
                            <h3>Suksesi HMJ Tahun 2019</h3>
                            <hr>
                            <p>Gambar ini Melsi sedang mengikuti kegiatan suksesi HMJ tahun 2019.</p>
                        </div>
                    </div>
                </div>
                <!--baris 8-->
                <div class="col-md-3">
                    <div class="single-service">
                        <img src="{{ asset('style/img/gal8.jpg') }}" alt="Card Image cap">
                        <div class="service-desc">
                            <h3>LPJ Akhir BEM 2020</h3>
                            <hr>
                            <p>Pada gambar ini, melsi sedang mengikuti kegiatan BEM Tahun 2020.</p>
                        </div>
                    </div>
                </div>

                <!--baris 7-->
                <div class="col-md-3">
                    <div class="single-service">
                        <img src="{{ asset('style/img/gal9.jpg') }}" alt="Card Image cap">
                        <div class="service-desc">
                            <h3>Sahabat</h3>
                            <hr>
                            <p>Gambar ini Melsi sedang berfoto dengan para sahabatnya.</p>
                        </div>
                    </div>
                </div>
                <!--baris 8-->
                <div class="col-md-3">
                    <div class="single-service">
                        <img src="{{ asset('style/img/gal10.jpg') }}" alt="Card Image cap">
                        <div class="service-desc">
                            <h3>Kakak Tingkat</h3>
                            <hr>
                            <p>Pada gambar ini, melsi sedang berfoto dengan kakak tingkat selaku wakil ketua BEM Tahun 2020.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
