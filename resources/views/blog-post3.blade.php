@extends('layout')

@section('judul')
    My Blog
@endsection

@section('konten')
    <!-- Page Title Starts -->
    <section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
        <h1>my <span>blog</span></h1>
        <span class="title-bg">posts</span>
    </section>
    <!-- Page Title Ends -->
    <!-- Main Content Starts -->
    <section class="blog-post3">
        <div class="container">
            <div class="row">
                <!-- Article Starts -->
                <article class="col-12">
                    <!-- Meta Starts -->
                    <div class="meta open-sans-font">
                        <span><i class="fa fa-user"></i> melsi</span>
                        <span class="date"><i class="fa fa-calendar"></i> 26 September 2021</span>
                        <span><i class="fa fa-tags"></i>melsi.com</span>
                    </div>
                    <!-- Meta Ends -->
                    <!-- Article Content Starts -->
                    <h1 class="text-uppercase text-capitalize">Cara Jitu Memiliki Banyak Teman</h1>
                    <img src="{{ asset('style/img/blog/blog-post-3.jpg') }}" class="img-fluid" alt="Blog image" />
                    <div class="blog-excerpt open-sans-font pb-5">
                        <p>Ikuti banyak kegiatan di kampus, seperti organisasi, maka kamu akan memiliki banyak
                            teman. Nah, bagi kamu yang cenderung pemalu dan terbatas dalam pergaulan, maka kamu bisa
                            mengikuti 5
                            cara berikut ini, agar dirimu bisa menemukan teman yang banyak. Untuk informasi lebih lanjut,
                            yuk simak ulasannya berikut ini! Belajar menerima diri sendiri lebih dulu. Hal pertama yang
                            harus kamu lakukan adalah belajar menerima diri apa adanya. Ingatlah bahwa
                            setiap orang memiliki kelebihan dan kekurangan, termasuk kamu. Bila kamu terus berfokus pada
                            kekurangan-kekurangan yang ada, akibatnya kamu akan dipenuhi dengan perasaan-perasaan negatif.
                            Contohnya minder, merasa tidak dihargai, merasa tidak diterima, dan sebagainya.</p>
                        <p>Percayalah, jika kamu cukup percaya diri, maka dirimu tidak akan kesulitan dalam menemukan
                            kelebihan dan kekurangan dengan seimbang. Tapi jika ternyata kamu masih kesulitan, maka dirimu
                            bisa mencoba lagi di lain waktu. Lakukanlah cara tersebut dengan rutin hingga dirimu mampu
                            menuliskan kelebihan dan kekurangan dengan seimbang. Dengan begitu, hal ini bisa menjadi bentuk
                            penerimaan diri dan kemampuan mencintai diri apa adanya. </p>
                        <p>Janganlah pernah untuk berpura-pura tidak tahu atau tidak dengar, bila ada teman yang kesusahan.
                            Malah sebisa mungkin kamulah orang pertama yang menolongnya atau menyampaikan kabar tersebut
                            kepada teman-teman yang lain. Percayalah, jika orang yang suka menolong itu tidak akan pernah
                            kekurangan teman. Pasalnya, akan
                            ada banyak orang yang nyaman bergaul denganmu karena sifatmu yang hangat dan menyenangkan.</p>
                    </div>
                    <!-- Article Content Ends -->
                </article>
                <!-- Article Ends -->
            </div>
        </div>
    </section>
@endsection
