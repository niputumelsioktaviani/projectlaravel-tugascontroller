@extends('layout')

@section('judul')
    About
@endsection

@section('konten')
    <section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
        <h1>ABOUT <span>ME</span></h1>
        <span class="title-bg">Resume</span>
    </section>
    <!-- Page Title Ends -->
    <!-- Main Content Starts -->
    <section class="container-fluid main-container container-about p-0 revealator-slideup revealator-once revealator-delay1">
        <div class="row">
            <div class="col-5">
                <img src="{{ asset('style/img/melsi.jpg') }}" class="img-about" alt="my picture" />
            </div>
            <!-- Personal Info Starts -->
            <div class="col-7">
                <div class="row mb-4">
                    <div class="col-12">
                        <h3 class="text-uppercase custom-title mb-0 ft-wt-600">Personal Info</h3>
                    </div>
                    <div class="col-6">
                        <ul class="about-list list-unstyled open-sans-font">
                            <li> <span class="title">first name :</span> <span
                                    class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Melsi</span>
                            </li>
                            <li> <span class="title">last name :</span> <span
                                    class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Oktaviani</span>
                            </li>
                            <li> <span class="title">Age :</span> <span
                                    class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">20
                                    Years</span> </li>
                            <li> <span class="title">Nationality :</span> <span
                                    class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Indonesia</span>
                            </li>
                            <li> <span class="title">Religion :</span> <span
                                    class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Hindu</span>
                            </li>
                        </ul>
                    </div>
                    <div class="col-6">
                        <ul class="about-list list-unstyled open-sans-font">
                            <li> <span class="title">Address :</span> <span
                                    class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Tajun
                                    Village</span>
                            </li>
                            <li> <span class="title">phone :</span> <span
                                    class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">+6281917173648</span>
                            </li>
                            <li> <span class="title">Email :</span> <span
                                    class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">melsi@undiksha.ac.id</span>
                            </li>
                            <li> <span class="title">Instagram :</span> <span
                                    class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">melsioktvni</span>
                            </li>
                            <li> <span class="title">langages :</span> <span
                                    class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Indonesia,
                                    English</span> </li>
                        </ul>
                    </div>
                    <div class="col-12 mt-3">
                        <a href="/about" class="btn btn-download">Download CV</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Personal Info Ends -->

        <hr class="separator">
        <!-- Skills Starts -->
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3 class="text-uppercase pb-4 pb-sm-5 mb-3 mb-sm-0 text-left text-sm-center custom-title ft-wt-600">
                        My Skills</h3>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p25">
                        <span>25%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">Menyanyi</h6>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p89">
                        <span>89%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">Menari Bali</h6>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p70">
                        <span>70%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">Menulis Cerita</h6>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p66">
                        <span>66%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">Speaking</h6>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p95">
                        <span>95%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">Membaca Novel</h6>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p50">
                        <span>50%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">Dance Moderen</h6>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p65">
                        <span>65%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">Acting</h6>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p45">
                        <span>45%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">Badminton</h6>
                </div>
            </div>
            <!-- Skills Ends -->
            <hr class="separator mt-1">
            <!-- Experience & Education Starts -->
            <div class="container-education">
                <div class="container position-relative d-flex align-items-center justify-content-center">
                    <h3 class="text-uppercase pb-5 mb-0 text-left text-sm-center custom-title ft-wt-600">
                        Learn More Education Me
                    </h3>
                </div>
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="border-start border-primary pt-2 pl-4 ml-2">
                            <!--baris pertama-->
                            <div class="position-relative ex-me mb-4">
                                <i class="far fa-dot-circle text-primary position-absolute"
                                    style="top: 2px; left: -35px; font-size: 22px;"></i>
                                <!--membuat circle-->
                                <h5 class="font-weight-bold mb-1 bg-danger">TK</h5>
                                <p class="mb-2"><strong>TK Kumara Bhakti</strong> | <small>2006 -
                                        2007</small></p>
                                <p>Saat masih kecil, Melsi Oktaviani disekolahkan oleh orang tuanya di TK Kumara Bhakti
                                    Desa Tajun
                                    pada tahun 2006 - 2007. Saat masih kecil pun Melsi Oktaviani memang sudah memiliki
                                    hobby membaca cerita.</p>
                            </div>
                            <!--baris kedua-->
                            <div class="position-relative ex-me mb-4">
                                <i class="far fa-dot-circle text-primary position-absolute"
                                    style="top: 2px; left: -35px; font-size: 22px;"></i>
                                <!--membuat circle-->
                                <h5 class="font-weight-bold mb-1 bg-danger">SD</h5>
                                <p class="mb-2"><strong>SD Negeri 4 Tajun</strong> | <small>2007 -
                                        2013</small></p>
                                <p>Saat lulus TK dari TK Kumara Bhakti Desa Tajun pada tahun 2007, Melsi melanjutkan
                                    sekolah di SD Negeri 4 Tajun sampai lulus. Saat bersekolah di SD Negeri
                                    4 Tajun Melsi Oktaviani sering mengunjungi perpustakaan. </p>
                            </div>
                            <!--baris ketiga-->
                            <div class="position-relative ex-me mb-4">
                                <i class="far fa-dot-circle text-primary position-absolute"
                                    style="top: 2px; left: -35px; font-size: 22px;"></i>
                                <!--membuat circle-->
                                <h5 class="font-weight-bold mb-1 bg-danger">SMP</h5>
                                <p class="mb-2"><strong>SMP Negeri 3 Kubutambahan</strong>
                                    |
                                    <small>2013 - 2016</small>
                                </p>
                                <p>Kemudian, Melsi Oktaviani melanjutkan sekolah menengah pertamanya di SMP Negeri 3
                                    Kubutambahan, Bali.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="border-start border-primary pt-2 pl-4 ml-2">
                            <!--baris kempat-->
                            <div class="position-relative ex-me mb-4">
                                <i class="far fa-dot-circle text-primary position-absolute"
                                    style="top: 2px; left: -35px; font-size: 22px;"></i>
                                <!--membuat circle-->
                                <h5 class="font-weight-bold mb-1 bg-danger">SMA</h5>
                                <p class="mb-2"><strong>SMA Negeri 2 Singaraja</strong>
                                    |
                                    <small>2016 - 2019</small>
                                </p>
                                <p>Sewaktu di SMA Negeri 2 Singaraja Melsi Oktaviani memang berprestrasi. Ia
                                    sampai dipindahkan ke kelas unggulan. Saat SMA Melsi Oktaviani merantau dari desa ke
                                    kota.
                                </p>
                            </div>
                            <!--baris kelima-->
                            <div class="position-relative ex-me mb-4">
                                <i class="far fa-dot-circle text-primary position-absolute"
                                    style="top: 2px; left: -35px; font-size: 22px;"></i>
                                <!--membuat circle-->
                                <h5 class="font-weight-bold mb-1 bg-danger">Kuliah S1</h5>
                                <p class="mb-2"><strong>Universitas Pendidikan Ganesha, Undiksha</strong> |
                                    <small>2019 -
                                        Sekarang</small>
                                </p>
                                <p>Saat lulus SMA, Melsi Oktaviani melanjutkan pendidikan S1 nya di Universitas
                                    Pendidikan Ganesha, Bali dengan mengambil jurusan Teknik Informatika, Sistem
                                    informasi. </p>
                            </div>
                            <!--baris ketiga-->
                            <div class="position-relative ex-me mb-4">
                                <i class="far fa-dot-circle text-primary position-absolute"
                                    style="top: 2px; left: -35px; font-size: 22px;"></i>
                                <!--membuat circle-->
                                <h5 class="font-weight-bold mb-1 bg-danger">Kuliah S2</h5>
                                <p class="mb-2"><strong>Universitas Udayana, Bali</strong> |
                                    <small>Setelah lulus S1</small>
                                </p>
                                <p>Kemudian, setelah lulus S1, rencananya Melsi akan melanjutkan pendidikan S2
                                    nya di UNUD, Jimbaran, Bali. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--akhir class container-education-->
            <!-- Experience & Education Ends -->
        </div>
    </section>
    <!-- Main Content Ends -->
@endsection
