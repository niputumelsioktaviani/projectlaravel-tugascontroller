@extends('layout')

@section('judul')
    My Blog
@endsection

@section('konten')
    <!-- Page Title Starts -->
    <section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
        <h1>my <span>blog</span></h1>
        <span class="title-bg">posts</span>
    </section>
    <!-- Page Title Ends -->
    <!-- Main Content Starts -->
    <section class="blog-post4">
        <div class="container">
            <div class="row">
                <!-- Article Starts -->
                <article class="col-12">
                    <!-- Meta Starts -->
                    <div class="meta open-sans-font">
                        <span><i class="fa fa-user"></i> melsi</span>
                        <span class="date"><i class="fa fa-calendar"></i> 26 September 2021</span>
                        <span><i class="fa fa-tags"></i>melsi.com</span>
                    </div>
                    <!-- Meta Ends -->
                    <!-- Article Content Starts -->
                    <h1 class="text-uppercase text-capitalize">Cara Ampuh Menang PMW</h1>
                    <img src="{{ asset('style/img/blog/blog-post-4.jpg') }}" class="img-fluid" alt="Blog image" />
                    <div class="blog-excerpt open-sans-font pb-5">
                        <p>Komponen utama dalam PMW adalah ide yang baru, menarik, kreatif, unik dan memiliki
                            manfaat dimasyarakat jika diapllikasikan. Yuk Cari Tahu lebih Jauh Cara
                            Ampuh Menang PMW. Galau, sedih, atau bahkan depresi akibat
                            proposal PMW tidak pernah sampai babak final pasti merupakan ujian tersendiri bagi para pejuang
                            proposal PMW. Pengorbanan yang tercurahkan seringkali tidak membuahkan hasil yang diharapkan.
                            Perasaan negatif ini bahkan lebih menjadi-jadi ketika kita melihat teman sendiri lolos dengan
                            lancarnya seakan tidak mengalami kendala seperti kita. Berdiam diri dan meratapi keadaan tidak
                            akan membuat satupun karya kita mendapat apresiasi. So, berdiri dan melangkahlah kedepan dengan
                            mantap. Agar dapat sampai ketujuan kita hanya perlu mengikuti jalan yang telah dilalui oleh
                            teman-teman kita yang pernah sampai “ketempat itu”. Seperti kata pepatah “You Have To Learn The
                            Rules Of The Game And Then You Have To Play Better Than Anyone Else”. Disini, penulis akan
                            memberikan tips dan trick buat kalian agar bisa membawa pulang piala di ajang lomba PMW.
                            So, Let’s learn guys!</p>
                        <p>Lomba PMM adalah sebuah karya tulis atau laporan yang diterbitkan berdasarkan
                            hasil penelitian/pemikiran yang dilakukan oleh seseorang atau tim yang telah memenuhi kaidah dan
                            etika keilmuan serta disusun dengan aturan ketat agar dapat memenuhi kaidah keilmuan sehingga
                            dapat berguna bagi masyarakat luas. Penyusunan proposal PMW harus sistematik, benar, utuh,
                            bertanggung jawab, serta berdasarkan kaidah bahasa yang benar. </p>
                        <p>Berdasarkan penjelasan yang telah diberikan dapat kita simpulkan bahwa komponen utama dalam
                            membuat proposal PMW adalah ide yang baru, menarik, kreatif, unik dan memiliki manfaat
                            dimasyarakat jika
                            diapllikasikan. Sekarang kita telah mengetahui kunci utama untuk meng-upgrade karya kita,
                            selanjutnya yang harus kita lakukan adalah mencari dan menggunakan kunci tersebut agar bisa
                            mengikuti jejak teman seperjuangan yang pernah merasakan sensai final PIMNAS. Selain PIMNAS ada
                            juga
                            perlombaan karya tulis yang terfokus ke Bussiness Plan. Universitas Pendidikan Ganesha sendiri
                            mengadakan perlombaan jenis ini dengan nama “PKM”.</p>
                    </div>
                    <!-- Article Content Ends -->
                </article>
                <!-- Article Ends -->
            </div>
        </div>
    </section>
@endsection
