<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from slimhamdi.net/tunis/dark/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Mar 2021 11:59:27 GMT -->

<head>
    <meta charset="utf-8">
    <title>@yield('judul') - Personal Portfolio</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Template Google Fonts -->
    <link
        href="{{ asset('style/../../../fonts.googleapis.com/cssdda2.css?family=Poppins:400,400i,500,500i,600,600i,700,700i,800,800i,900,900i') }}"
        rel="stylesheet">
    <link
        href="{{ asset('style/../../../fonts.googleapis.com/cssab6d.css?family=Open+Sans:300,400,400i,600,600i,700') }}"
        rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">

    <!-- FonsAwesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous">
    </script>

    <!-- Template CSS Files -->
    <link href="{{ asset('style/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('style/css/preloader.min.css') }}" rel="stylesheet">
    <link href="{{ asset('style/css/circle.css') }}" rel="stylesheet">
    <link href="{{ asset('style/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('style/css/fm.revealator.jquery.min.css') }}" rel="stylesheet">
    <link href="{{ asset('style/css/style.css') }}" rel="stylesheet">

    <!-- CSS Skin File -->
    <link href="{{ asset('style/css/skins/red.css') }}" rel="stylesheet">

    <!-- Live Style Switcher - demo only -->
    <link rel="alternate stylesheet" type="text/css" title="blue" href="{{ asset('style/css/skins/blue.css') }}" />
    <link rel="alternate stylesheet" type="text/css" title="green" href="{{ asset('style/css/skins/green.css') }}" />
    <link rel="alternate stylesheet" type="text/css" title="yellow"
        href="{{ asset('style/css/skins/yellow.css') }}" />
    <link rel="alternate stylesheet" type="text/css" title="blueviolet"
        href="{{ asset('style/css/skins/blueviolet.css') }}" />
    <link rel="alternate stylesheet" type="text/css" title="goldenrod"
        href="{{ asset('style/css/skins/goldenrod.css') }}" />
    <link rel="alternate stylesheet" type="text/css" title="magenta"
        href="{{ asset('style/css/skins/magenta.css') }}" />
    <link rel="alternate stylesheet" type="text/css" title="orange"
        href="{{ asset('style/css/skins/orange.css') }}" />
    <link rel="alternate stylesheet" type="text/css" title="purple"
        href="{{ asset('style/css/skins/purple.css') }}" />
    <link rel="alternate stylesheet" type="text/css" title="red" href="{{ asset('style/css/skins/red.css') }}" />
    <link rel="alternate stylesheet" type="text/css" title="yellowgreen"
        href="{{ asset('style/css/skins/yellowgreen.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('style/css/styleswitcher.css') }}" />

    <!-- Modernizr JS File -->
    <script src="{{ asset('style/js/modernizr.custom.js') }}"></script>
</head>

<body class="about" class="home">
    <!-- Live Style Switcher Starts - demo only -->
    <div id="switcher" class="">
    <div class=" content-switcher">
        <h4>Pilih Warna</h4>
        <ul>
            <li>
                <a href="#" onclick="setActiveStyleSheet('purple');" title="purple" class="color"><img
                        src="{{ asset('style/img/styleswitcher/purple.png') }}" alt="purple" /></a>
            </li>
            <li>
                <a href="#" onclick="setActiveStyleSheet('red');" title="red" class="color"><img
                        src="{{ asset('style/img/styleswitcher/red.png') }}" alt="red" /></a>
            </li>
            <li>
                <a href="#" onclick="setActiveStyleSheet('blueviolet');" title="blueviolet" class="color"><img
                        src="{{ asset('style/img/styleswitcher/blueviolet.png') }}" alt="blueviolet" /></a>
            </li>
            <li>
                <a href="#" onclick="setActiveStyleSheet('blue');" title="blue" class="color"><img
                        src="{{ asset('style/img/styleswitcher/blue.png') }}" alt="blue" /></a>
            </li>
            <li>
                <a href="#" onclick="setActiveStyleSheet('goldenrod');" title="goldenrod" class="color"><img
                        src="{{ asset('style/img/styleswitcher/goldenrod.png') }}" alt="goldenrod" /></a>
            </li>
            <li>
                <a href="#" onclick="setActiveStyleSheet('magenta');" title="magenta" class="color"><img
                        src="{{ asset('style/img/styleswitcher/magenta.png') }}" alt="magenta" /></a>
            </li>
            <li>
                <a href="#" onclick="setActiveStyleSheet('yellowgreen');" title="yellowgreen" class="color"><img
                        src="{{ asset('style/img/styleswitcher/yellowgreen.png') }}" alt="yellowgreen" /></a>
            </li>
            <li>
                <a href="#" onclick="setActiveStyleSheet('orange');" title="orange" class="color"><img
                        src="{{ asset('style/img/styleswitcher/orange.png') }}" alt="orange" /></a>
            </li>
            <li>
                <a href="#" onclick="setActiveStyleSheet('green');" title="green" class="color"><img
                        src="{{ asset('style/img/styleswitcher/green.png') }}" alt="green" /></a>
            </li>
            <li>
                <a href="#" onclick="setActiveStyleSheet('yellow');" title="yellow" class="color"><img
                        src="{{ asset('style/img/styleswitcher/yellow.png') }}" alt="yellow" /></a>
            </li>
        </ul>

        <a href="https://themeforest.net/item/tunis-personal-portfolio/26761598?irgwc=1&amp;clickid=UBs0rDX6YxyJWfewUx0Mo3E1UkiwozXD10XATw0&amp;iradid=275988&amp;irpid=1327395&amp;iradtype=ONLINE_TRACKING_LINK&amp;irmptype=mediapartner&amp;mp_value1=&amp;utm_campaign=af_impact_radius_1327395&amp;utm_medium=affiliate&amp;utm_source=impact_radius"
            class="waves-effect waves-light font-weight-700 purchase"><i class="fa fa-shopping-cart"></i> Purchase</a>
        <div id="hideSwitcher">&times;</div>
    </div>
    </div>
    <div id="showSwitcher" class="styleSecondColor"><i class="fa fa-cog fa-spin"></i></div>
    <!-- Live Style Switcher Ends - demo only -->
    <!-- Header Starts -->
    <header class="header" id="navbar-collapse-toggle">
        <!-- Fixed Navigation Starts -->
        <ul class="icon-menu d-none d-lg-block revealator-slideup revealator-once revealator-delay1">
            <li class="icon-box">
                <i class="fa fa-home"></i>
                <a href="/">
                    <h2>Home</h2>
                </a>
            </li>
            <li class="icon-box">
                <i class="fa fa-user"></i>
                <a href="/about">
                    <h2>About</h2>
                </a>
            </li>
            <li class="icon-box">
                <i class="fa fa-briefcase"></i>
                <a href="/portfolio">
                    <h2>Portfolio</h2>
                </a>
            </li>
            <li class="icon-box">
                <i class="fa fa-envelope-open"></i>
                <a href="/contact">
                    <h2>Contact</h2>
                </a>
            </li>
            <li class="icon-box">
                <i class="fa fa-comments"></i>
                <a href="/blog">
                    <h2>Blog</h2>
                </a>
            </li>
        </ul>
        <!-- Fixed Navigation Ends -->
        <!-- Mobile Menu Starts -->
        <nav role="navigation" class="d-block d-lg-none">
            <div id="menuToggle">
                <input type="checkbox" />
                <span></span>
                <span></span>
                <span></span>
                <ul class="list-unstyled" id="menu">
                    <li><a href="/"><i class="fa fa-home"></i><span>Home</span></a></li>
                    <li><a href="/about"><i class="fa fa-user"></i><span>About</span></a>
                    </li>
                    <li><a href="/portfolio"><i class="fa fa-folder-open"></i><span>Portfolio</span></a></li>
                    <li><a href="/contact"><i class="fa fa-envelope-open"></i><span>Contact</span></a></li>
                    <li><a href="/blog"><i class="fa fa-comments"></i><span>Blog</span></a></li>
                </ul>
            </div>
        </nav>
        <!-- Mobile Menu Ends -->
    </header>
    <!-- Header Ends -->
    <!-- Page Title Starts -->
    <!-- Main Content Start -->
    @yield('konten')
    <!-- Main Content Ends -->

    <!-- Template JS Files -->
    <script src="{{ asset('style/js/jquery-3.5.0.min.js') }}"></script>
    <script src="{{ asset('style/js/styleswitcher.js') }}"></script>
    <script src="{{ asset('style/js/preloader.min.js') }}"></script>
    <script src="{{ asset('style/js/fm.revealator.jquery.min.js') }}"></script>
    <script src="{{ asset('style/js/imagesloaded.pkgd.min.js') }}"></script>
    <script src="{{ asset('style/js/masonry.pkgd.min.js') }}"></script>
    <script src="{{ asset('style/js/classie.js') }}"></script>
    <script src="{{ asset('style/js/cbpGridGallery.js') }}"></script>
    <script src="{{ asset('style/js/jquery.hoverdir.js') }}"></script>
    <script src="{{ asset('style/js/popper.min.js') }}"></script>
    <script src="{{ asset('style/js/bootstrap.js') }}"></script>
    <script src="{{ asset('style/js/custom.js') }}"></script>

</body>


<!-- Mirrored from slimhamdi.net/tunis/dark/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Mar 2021 11:59:27 GMT -->

</html>
